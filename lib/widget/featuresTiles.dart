import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:second_task/color.dart';

class FeaturesList extends StatelessWidget {
  final String title;

  const FeaturesList({
    Key key,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 5,
      ),
      child: Row(
        children: [
          Icon(
            Icons.check_circle_outline,
            size: ScreenUtil().setSp(18),
            color: AppColors.indigo.withOpacity(0.6),
          ),
          const SizedBox(
            width: 10,
          ),
          Text(
            title,
            style: TextStyle(
              fontSize: ScreenUtil().setSp(15),
              fontWeight: FontWeight.w400,
              color: AppColors.indigo.withOpacity(0.6),
            ),
          ),
        ],
      ),
    );
  }
}
