import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:second_task/color.dart';
import 'package:second_task/webview.dart';

import 'alertDialog.dart';

class BoardingList extends StatelessWidget {
  final String image;
  final String title;
  final String subTitle;
  final String url;

  const BoardingList({
    Key key,
    this.image,
    this.title,
    this.subTitle,
    this.url,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: (){
        showDialog(context: context, builder: (context){
          return AlertNotice(
            title: "Confirmation",
            contentText: "The link/button clicked will be opened in browser.\nThis dialog will show only on interactive demo/prototype",
            buttonText: "Confirm",
            onPressed: (){
              Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => BrowserView(
                    url: url,
                  ),
                ),
              );
            },
          );
        });
      },
      contentPadding: const EdgeInsets.symmetric(
        horizontal: 30,
      ),
      leading: Image.asset(
        image,
      ),
      title: Row(
        children: [
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: ScreenUtil().setSp(18),
              color: AppColors.brilliantAzure,
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          Icon(
            Icons.north_east,
            size: ScreenUtil().setSp(18),
            color: AppColors.accent,
          ),
        ],
      ),
      subtitle: Text(
        subTitle,
        style: TextStyle(
          fontSize: ScreenUtil().setSp(14),
          fontWeight: FontWeight.w400,
          color: AppColors.indigo,
        ),
        textAlign: TextAlign.start,
      ),
    );
  }
}
