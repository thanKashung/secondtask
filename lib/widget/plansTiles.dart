import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:second_task/color.dart';

class PlansTiles extends StatelessWidget {
  final String title;
  final String features;
  final String price;

  const PlansTiles({
    Key key,
    this.title,
    this.features,
    this.price,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){

      },
      child: Container(
        width: MediaQuery.of(context).size.width / 3.6,
        height: ScreenUtil().setHeight(120),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: title == 'Premium' ? AppColors.accent : AppColors.grey,
            width: 1,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: ScreenUtil().setSp(16),
                  color: title == 'Premium' ? AppColors.accent : AppColors.grey,
                ),
              ),
              Text(
                features,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(12),
                  color: title == 'Premium' ? AppColors.accent : AppColors.grey,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children:[
                    Text(
                      "\$$price",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(13),
                        color: title == 'Premium' ? AppColors.accent : AppColors.grey,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Text(
                      "per month",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(12),
                        color: title == 'Premium' ? AppColors.accent : AppColors.grey,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
