import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:second_task/color.dart';

class AlertNotice extends StatelessWidget {
  final String title;
  final String contentText;
  final String buttonText;
  final Function onPressed;

  const AlertNotice({
    Key key,
    this.title,
    this.contentText,
    this.buttonText,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),

      title: Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            title,
          ),
        ),
      ),
      titleTextStyle: TextStyle(
        fontSize: ScreenUtil().setSp(22),
        fontWeight: FontWeight.w500,
        color: AppColors.brilliantAzure,
      ),
      content: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
        ),
        child: Text(
          contentText,
          textAlign: TextAlign.justify,
        ),
      ),
      contentTextStyle: TextStyle(
        fontSize: ScreenUtil().setSp(16),
        fontWeight: FontWeight.w400,
        color: AppColors.brilliantAzure,
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: InkWell(
            onTap: onPressed,
            child: Container(
              padding: const EdgeInsets.symmetric(
                vertical: 7,
                horizontal: 100,
              ),
              decoration: BoxDecoration(
                color: AppColors.accent.withOpacity(0.3),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Text(
                buttonText,
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(15),
                  fontWeight: FontWeight.w500,
                  color: AppColors.accent,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
