import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:second_task/color.dart';
import 'dart:io';
import 'package:webview_flutter/webview_flutter.dart';

class BrowserView extends StatefulWidget {
  final String url;

  const BrowserView({
    Key key,
    this.url,
  }) : super(key: key);

  @override
  State<BrowserView> createState() => _BrowserViewState();
}

class _BrowserViewState extends State<BrowserView> {
  String url;
  int selectedValue = 1;

  @override
  void initState() {
    url = widget.url;
    super.initState();
    if (Platform.isAndroid) {
      WebView.platform = AndroidWebView();
    } else if (Platform.isIOS) {
      WebView.platform = CupertinoWebView();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(20.0),
        child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.only(
                top: ScreenUtil().setHeight(80),
              ),
              child: WebView(
                initialUrl: url,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  "assets/images/blog_logo.png",
                  height: ScreenUtil().setWidth(100),
                  width: ScreenUtil().setWidth(100),
                ),
                Row(
                  children: [
                    Icon(
                      Icons.format_align_right,
                      size: ScreenUtil().setSp(20),
                      color: AppColors.indigo,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        children: [
                          Text(
                            "EN",
                            style: TextStyle(
                              fontSize: ScreenUtil().setSp(20),
                              color: AppColors.indigo,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Icon(
                            Icons.expand_more,
                            size: ScreenUtil().setSp(20),
                            color: AppColors.indigo,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
