import 'package:flutter/material.dart';

class AppColors{
  static const Color white = Color(0xFFFAFCFF);
  static const Color accent = Color(0xFF3683FC);
  static const Color brilliantAzure = Color(0xFF0C1E3C);
  static const Color indigo = Color(0xFF21355C);
  static const Color water = Color(0xFFCDE0FE);
  static const Color grey = Color(0xFF707D8B);
}