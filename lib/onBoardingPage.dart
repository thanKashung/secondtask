import 'dart:ui';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:second_task/color.dart';
import 'package:second_task/contactUs.dart';
import 'package:second_task/webview.dart';
import 'package:second_task/widget/alertDialog.dart';
import 'package:second_task/widget/boardingList.dart';

class OnBoardingPage extends StatefulWidget {
  const OnBoardingPage({Key key}) : super(key: key);

  @override
  State<OnBoardingPage> createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  final GlobalKey<ScaffoldState> scaffoldStateKey = GlobalKey();
  final PageController _controller = PageController();
  final PageController _mainController = PageController();
  int _current = 0;
  bool tapped = false;

  @override
  Widget build(BuildContext context) {

    List<Widget> screens = [
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.swipe_left_sharp,
                    size: ScreenUtil().setSp(14),
                    color: AppColors.indigo,
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    "Swipe To Continue",
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: AppColors.indigo,
                      fontSize: ScreenUtil().setSp(13),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                "Welcome!",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: AppColors.accent,
                  fontSize: ScreenUtil().setSp(30),
                ),
              ),
            ],
          ),
          Image.asset(
            "assets/images/onBoarding1.png",
            height: ScreenUtil().setWidth(230),
            width: ScreenUtil().setWidth(230),
            fit: BoxFit.contain,
          ),
          Column(
            children: [
              Text(
                "Welcome To The Only\nOne Of It's Kind",
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(27),
                  color: AppColors.indigo,
                ),
                textAlign: TextAlign.center,
              ),
              Text(
                "AI Health Assistant",
                style: TextStyle(
                  fontSize: ScreenUtil().setSp(27),
                  fontWeight: FontWeight.w700,
                  color: AppColors.indigo,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 10,
              ),
              TextButton(
                onPressed: () {
                  _mainController.nextPage(
                    duration: const Duration(milliseconds: 10),
                    curve: Curves.linear,
                  );
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Skip",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(20),
                        fontWeight: FontWeight.w400,
                        color: AppColors.accent,
                      ),
                    ),
                    Icon(
                      Icons.chevron_right,
                      size: ScreenUtil().setSp(25),
                      color: AppColors.accent,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            "AI Powered",
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: AppColors.accent,
              fontSize: ScreenUtil().setSp(32),
            ),
          ),
          Image.asset(
            "assets/images/onBoarding2.png",
            height: ScreenUtil().setWidth(200),
            width: ScreenUtil().setWidth(285),
            fit: BoxFit.contain,
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  "A Digital BioBank with risk assessments and personalized recommendations that can predict severe diseases and extend your health span",
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(18),
                    color: AppColors.indigo,
                    fontWeight: FontWeight.w400,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertNotice(
                          title: "Confirmation",
                          contentText:
                              "The link/button clicked will be opened in browser.\nThis dialog will show only on interactive demo/prototype",
                          buttonText: "Confirm",
                          onPressed: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const BrowserView(
                                  url: "http://blog.longevityintime.org",
                                ),
                              ),
                            );
                          },
                        );
                      });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "see how it works",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        fontWeight: FontWeight.w400,
                        color: AppColors.accent,
                      ),
                    ),
                    Icon(
                      Icons.north_east,
                      size: ScreenUtil().setSp(16),
                      color: AppColors.accent,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            "Data Protected",
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: AppColors.accent,
              fontSize: ScreenUtil().setSp(32),
            ),
          ),
          Image.asset(
            "assets/images/onBoarding3.png",
            height: ScreenUtil().setWidth(200),
            width: ScreenUtil().setWidth(285),
            fit: BoxFit.contain,
          ),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Text(
                  "We use the latest encryption technology and anonymization to ensure your data will be secure in our servers at and Amazon (AWS)",
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(18),
                    color: AppColors.indigo,
                    fontWeight: FontWeight.w400,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertNotice(
                          title: "Confirmation",
                          contentText:
                              "The link/button clicked will be opened in browser.\nThis dialog will show only on interactive demo/prototype",
                          buttonText: "Confirm",
                          onPressed: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const BrowserView(
                                  url: "blog.longevityintime.org",
                                ),
                              ),
                            );
                          },
                        );
                      });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "learn more",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        fontWeight: FontWeight.w400,
                        color: AppColors.accent,
                      ),
                    ),
                    Icon(
                      Icons.north_east,
                      size: ScreenUtil().setSp(16),
                      color: AppColors.accent,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(
            "Live Longer and\nearn LONG InTime",
            style: TextStyle(
              fontWeight: FontWeight.w700,
              color: AppColors.accent,
              fontSize: ScreenUtil().setSp(32),
            ),
            textAlign: TextAlign.center,
          ),
          Image.asset(
            "assets/images/onBoarding4.png",
            height: ScreenUtil().setWidth(200),
            width: ScreenUtil().setWidth(285),
            fit: BoxFit.contain,
          ),
          Column(
            children: [
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Text(
                  "Add parameters, walk everyday, play our game and invite friends to get achievements  and be rewarded in \$LONG token",
                  style: TextStyle(
                    fontSize: ScreenUtil().setSp(18),
                    color: AppColors.indigo,
                    fontWeight: FontWeight.w400,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return AlertNotice(
                          title: "Confirmation",
                          contentText:
                              "The link/button clicked will be opened in browser.\nThis dialog will show only on interactive demo/prototype",
                          buttonText: "Confirm",
                          onPressed: () {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const BrowserView(
                                  url: "blog.longevityintime.org",
                                ),
                              ),
                            );
                          },
                        );
                      });
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "earn more",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(16),
                        fontWeight: FontWeight.w400,
                        color: AppColors.accent,
                      ),
                    ),
                    Icon(
                      Icons.north_east,
                      size: ScreenUtil().setSp(16),
                      color: AppColors.accent,
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Text(
                "What else we do",
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  color: AppColors.accent,
                  fontSize: ScreenUtil().setSp(32),
                ),
              ),
              Text(
                'We like to call it \n"The Longevity Ecosystem"',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  color: AppColors.indigo,
                  fontSize: ScreenUtil().setSp(16),
                ),
              ),
            ],
          ),
          ListView(
            shrinkWrap: true,
            children: const [
              BoardingList(
                image: 'assets/images/hNet.png',
                title: 'Health Network',
                subTitle:
                    'Connect doctors and patients using our API to get faster diagnosis',
                url: "blog.longevityintime.org",
              ),
              SizedBox(
                height: 20,
              ),
              BoardingList(
                image: 'assets/images/resort.png',
                title: 'Scientific Resort',
                subTitle: 'Time off with professional treatments',
                url: "blog.longevityintime.org",
              ),
              SizedBox(
                height: 20,
              ),
              BoardingList(
                image: 'assets/images/crypto.png',
                title: 'Crypto',
                subTitle: 'A token and a NFT collection of human organs',
                url: "https://longevitycoin.org/",
              ),
              SizedBox(
                height: 20,
              ),
              BoardingList(
                image: 'assets/images/gaming.png',
                title: 'Gaming',
                subTitle: 'Your virtual self in from your real parameters',
                url: "blog.longevityintime.org",
              ),
              SizedBox(
                height: 20,
              ),
              BoardingList(
                image: 'assets/images/petition.png',
                title: 'Petition',
                subTitle: 'Support fundamental anti-aging researches',
                url: "blog.longevityintime.org",
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    ];

    List<String> wavesLayer = [
      "assets/images/waves1.png",
      "assets/images/waves2.png",
      "assets/images/waves3.png",
      "assets/images/waves4.png",
      "assets/images/waves5.png",
      "assets/images/waves6.png",
    ];

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: scaffoldStateKey,
        body: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: _mainController,
          children: [
            Stack(
              children: [
                PageView(
                  pageSnapping: false,
                  physics: const NeverScrollableScrollPhysics(),
                  controller: _controller,
                  children: screens,
                  onPageChanged: (index) {
                    setState(() {
                      _current = index;
                    });
                  },
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    height: ScreenUtil().setHeight(80),
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                          wavesLayer[_current],
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 30,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: screens.asMap().entries.map((entry) {
                              return GestureDetector(
                                onTap: () => _controller.animateToPage(
                                  entry.key,
                                  duration: const Duration(milliseconds: 10),
                                  curve: Curves.linear,
                                ),
                                child: Container(
                                  width: 25.0,
                                  height: 5.0,
                                  margin: const EdgeInsets.symmetric(
                                    vertical: 8.0,
                                    horizontal: 4.0,
                                  ),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(50),
                                    color: _current == entry.key
                                        ? AppColors.white
                                        : AppColors.white.withOpacity(0.6),
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                          TextButton(
                            onPressed: () {
                              if (_current == 4) {
                                setState(() {
                                  _current++;
                                });
                                _mainController.nextPage(
                                  duration: const Duration(milliseconds: 10),
                                  curve: Curves.easeInSine,

                                );
                              } else {
                                setState(() {
                                  _current++;
                                });
                                _controller.nextPage(
                                  duration: const Duration(milliseconds: 10),
                                  curve: Curves.easeInSine,
                                );
                              }
                            },
                            child: Text(
                              _current == 4 ? "Finish" : "Next",
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(18),
                                fontWeight: FontWeight.w600,
                                color: AppColors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Stack(
              children: [
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    height: ScreenUtil().setHeight(80),
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10,
                    ),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage(
                          wavesLayer[_current],
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        Text(
                          "Know about us more",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            color: AppColors.accent,
                            fontSize: ScreenUtil().setSp(28),
                          ),
                        ),
                        Text(
                          "Get to know more about what\nwe do by watching a short video",
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: AppColors.indigo,
                            fontSize: ScreenUtil().setSp(16),
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                    Card(
                      elevation: 5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: ClipRect(
                        child: Container(
                          height: MediaQuery.of(context).size.height / 1.8,
                          width: MediaQuery.of(context).size.width / 1.1,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: const DecorationImage(
                              image: AssetImage(
                                "assets/images/video.png",
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: BackdropFilter(
                            filter: ImageFilter.blur(
                              sigmaX: 8.0,
                              sigmaY: 8.0,
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.grey.withOpacity(0.3),
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Center(
                                child: InkWell(
                                  onTap: () {},
                                  child: Card(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(100)),
                                    elevation: 5,
                                    color: Colors.white.withOpacity(0.7),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Icon(
                                        Icons.play_arrow_rounded,
                                        color: AppColors.accent,
                                        size: ScreenUtil().setSp(70),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              tapped = true;
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 50,
                            ),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(30),
                                color: AppColors.accent,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 80,
                                  vertical: 10,
                                ),
                                child: Center(
                                  child: tapped
                                      ? const SizedBox(
                                    width: 23,
                                    height: 23,
                                    child: CircularProgressIndicator(
                                      color: AppColors.white,
                                      strokeWidth: 3,
                                    ),
                                  )
                                      : Text(
                                    "Let's Go",
                                    style: TextStyle(
                                      fontSize: ScreenUtil().setSp(20),
                                      color: AppColors.white,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 10,
                            horizontal: 10,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                "Not so sure yet?",
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.indigo,
                                  fontSize: ScreenUtil().setSp(15),
                                ),
                                textAlign: TextAlign.center,
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              InkWell(
                                onTap: () {
                                  scaffoldStateKey.currentState
                                      .showBottomSheet((context) {
                                    return Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Colors.black.withOpacity(0),
                                        boxShadow: [
                                          BoxShadow(
                                            color: AppColors.grey.withOpacity(0.4),
                                            blurRadius: 50,
                                            spreadRadius: 50,
                                            offset: const Offset(0.3, 0.5),
                                          ),
                                        ],
                                      ),
                                      padding: const EdgeInsets.all(0),
                                      child: Card(
                                        shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(20),
                                            topLeft: Radius.circular(20),
                                          ),
                                        ),
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            Center(
                                              child: Padding(
                                                padding: const EdgeInsets.all(8.0),
                                                child: Container(
                                                  color: AppColors.grey
                                                      .withOpacity(0.3),
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width /
                                                      3,
                                                  height: 5,
                                                ),
                                              ),
                                            ),
                                            Center(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                  top: 50,
                                                  bottom: 30,
                                                ),
                                                child: Text(
                                                  "Demo Mode",
                                                  style: TextStyle(
                                                    color: AppColors.accent,
                                                    fontSize: ScreenUtil().setSp(23),
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.symmetric(
                                                horizontal: 20,
                                              ),
                                              child: Column(
                                                children: [
                                                  Text(
                                                    "In demo mode, you can try prediction features without creating an account or inputing data. \n\nUsing a random preset of full parameters, we will make real time predictions and give recommendations\n",
                                                    style: TextStyle(
                                                      color: AppColors.indigo,
                                                      fontSize: ScreenUtil().setSp(16),
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ),
                                                  Text(
                                                    "Demo mode is not intended for personal use or give recommendations for your parameters, All data provided here has no relation with users, any similarity with personal information is mere coincidence.\n\n",
                                                    style: TextStyle(
                                                      color: AppColors.grey,
                                                      fontFamily: 'DMSans-Italic',
                                                      fontSize: ScreenUtil().setSp(16),
                                                      fontWeight: FontWeight.w400,
                                                    ),
                                                  ),
                                                  RichText(
                                                    text: TextSpan(
                                                      text:
                                                      "By proceeding, you agree with our",
                                                      style: TextStyle(
                                                        color: AppColors.grey,
                                                        fontSize: ScreenUtil().setSp(16),
                                                        fontWeight: FontWeight.w400,
                                                      ),
                                                      children: [
                                                        TextSpan(
                                                          text:
                                                          ' additional terms of service for demo mode',
                                                          style: TextStyle(
                                                            color: AppColors.accent,
                                                            fontSize: ScreenUtil().setSp(16),
                                                            fontWeight:
                                                            FontWeight.w400,
                                                          ),
                                                          recognizer:
                                                          TapGestureRecognizer()
                                                            ..onTap = () {
                                                              scaffoldStateKey
                                                                  .currentState
                                                                  .showBottomSheet(
                                                                      (context) =>
                                                                      Container());
                                                            },
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.symmetric(
                                                horizontal: 25,
                                                vertical: 35,
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                                mainAxisSize: MainAxisSize.max,
                                                children: [
                                                  InkWell(
                                                    onTap: () {
                                                      Navigator.of(context).pop();
                                                    },
                                                    borderRadius:
                                                    BorderRadius.circular(30),
                                                    child: Container(
                                                      padding: const EdgeInsets
                                                          .symmetric(
                                                        horizontal: 30,
                                                        vertical: 10,
                                                      ),
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                        BorderRadius.circular(
                                                            30),
                                                        border: Border.all(
                                                          color: AppColors.accent,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      child: Text(
                                                        "Go Back",
                                                        style: TextStyle(
                                                          fontSize: ScreenUtil().setSp(16),
                                                          color: AppColors.accent,
                                                          fontWeight:
                                                          FontWeight.w500,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  InkWell(
                                                    onTap: (){
                                                      Navigator.pop(context);
                                                      Navigator.of(context).push(
                                                        MaterialPageRoute(
                                                          builder: (context) => const ContactPage(),
                                                        ),
                                                      );
                                                    },
                                                    borderRadius: BorderRadius.circular(30),
                                                    child: Container(
                                                      padding:
                                                      const EdgeInsets.symmetric(
                                                        horizontal: 30,
                                                        vertical: 10,
                                                      ),
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                        BorderRadius.circular(30),
                                                        color: AppColors.accent,
                                                      ),
                                                      child: Text(
                                                        "Try It Now",
                                                        style: TextStyle(
                                                          fontSize: ScreenUtil().setSp(16),
                                                          color: AppColors.white,
                                                          fontWeight: FontWeight.w500,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  });
                                },
                                child: Text(
                                  "Book a demo",
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(15),
                                    color: AppColors.accent,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
