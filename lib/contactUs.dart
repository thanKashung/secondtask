import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:second_task/color.dart';
import 'package:second_task/widget/featuresTiles.dart';
import 'package:second_task/widget/plansTiles.dart';

class ContactPage extends StatefulWidget {
  const ContactPage({Key key}) : super(key: key);

  @override
  State<ContactPage> createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
            ),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const SizedBox(
                    height: 70,
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 30,
                          right: 30,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                "Features",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(22),
                                  fontWeight: FontWeight.w600,
                                  color: AppColors.accent,
                                ),
                              ),
                            ),
                            Column(
                              children: const [
                                FeaturesList(
                                  title: 'Patient Data Depersonalization',
                                ),
                                FeaturesList(
                                  title: 'Clinical trials AI simulation',
                                ),
                                FeaturesList(
                                  title: 'Predictions endpoints',
                                ),
                                FeaturesList(
                                  title: 'Document analysis (lab test)',
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 10,
                          right: 10,
                        ),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Text(
                                "Plans available",
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(22),
                                  fontWeight: FontWeight.w600,
                                  color: AppColors.accent,
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: const[
                                PlansTiles(
                                  title: 'Basic',
                                  features: 'Single feature available',
                                  price: '5',
                                ),
                                PlansTiles(
                                  title: 'Premium',
                                  features: 'Two features available',
                                  price: '15',
                                ),
                                PlansTiles(
                                  title: 'Ultra',
                                  features: 'All features available',
                                  price: '25',
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Contact",
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(18),
                                fontWeight: FontWeight.w400,
                                color: AppColors.indigo,
                              ),
                            ),
                            Text(
                              "Connect with us to know more about our product",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(28),
                                fontWeight: FontWeight.w800,
                                color: AppColors.accent,
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Dba. Longevity InTime\n300 Delaware Avenue, Suite 210-A,\nWilmington, Delaware, 19801, USA",
                              style: TextStyle(
                                fontSize: ScreenUtil().setSp(15),
                                color: AppColors.indigo,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 5,
                              ),
                              child: Text(
                                "Get directions",
                                style: TextStyle(
                                  fontSize: ScreenUtil().setSp(15),
                                  color: AppColors.indigo,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "contact@intime.digital",
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(13),
                                    color: AppColors.indigo,
                                  ),
                                ),
                                Text(
                                  "010 2678 2189",
                                  style: TextStyle(
                                    fontSize: ScreenUtil().setSp(13),
                                    color: AppColors.indigo,
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                InkWell(
                                  onTap: () {},
                                  radius: 1,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      color: AppColors.accent,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Icon(
                                        Icons.mail,
                                        size: ScreenUtil().setSp(20),
                                        color: AppColors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 10,
                                ),
                                InkWell(
                                  onTap: () {},
                                  radius: 1,
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      color: AppColors.accent,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(10.0),
                                      child: Icon(
                                        Icons.call,
                                        size: ScreenUtil().setSp(20),
                                        color: AppColors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 30,
              left: 10,
            ),
            child: InkWell(
              onTap: (){
                Navigator.of(context).pop();
              },
              splashColor: AppColors.accent.withOpacity(0.5),
              borderRadius: BorderRadius.circular(100),
              child: Icon(
                Icons.keyboard_arrow_left_outlined,
                color: AppColors.indigo,
                size: ScreenUtil().setSp(50),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
