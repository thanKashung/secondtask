import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:second_task/color.dart';
import 'package:second_task/onBoardingPage.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'homePage.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({
    Key key,
  }) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    checkSharedPreferences();
  }

  void checkSharedPreferences() async{
    final prefs = await SharedPreferences.getInstance();
    final showHome = prefs.getBool('showHome') ?? false;
    Timer(
      const Duration(seconds: 3),
          () => Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => const OnBoardingPage(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const SizedBox(
              height: 10,
            ),
            Center(
              child: Image.asset(
                "assets/images/logo.png",
                height: ScreenUtil().setWidth(160),
                width: ScreenUtil().setWidth(160),
                fit: BoxFit.contain,
                filterQuality: FilterQuality.high,
              ),
            ),
            Column(
              children: [
                const CircularProgressIndicator(
                  color: AppColors.accent,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                   vertical: 40,
                  ),
                  child: Text(
                    "Disease Tracker",
                    style: TextStyle(
                      fontSize: ScreenUtil().setSp(18),
                      fontWeight: FontWeight.w400,
                      color: AppColors.accent,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
